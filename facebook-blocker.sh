#!/bin/bash

# Copyright (C) 2014 Ivaylo Valkov <ivaylo@e-valkov.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# this program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


# This script fetches the IP addresses that Facebook uses and inserts
# iptables rules to block them, so Facebook cannot access the server
# where the rules are applied.

# Facebook provides a way to extract all IP addresses that they use.
# https://developers.facebook.com/docs/opengraph/howtos/maximizing-distribution-media-content#crawl 

# Best run in Crontab:
# Once a week in Monday
# 20 0    * * 1   root  /path/to/script/facebook-block.sh


iptables_rules=/tmp/iptables-rules
ip6tables_rules=/tmp/ip6tables-rules
sudo iptables-save > ${iptables_rules}
sudo ip6tables-save > ${ip6tables_rules}

# https://developers.facebook.com/docs/opengraph/howtos/maximizing-distribution-media-content#crawl 
whois -h whois.radb.net -- '-i origin AS32934' | grep ^route |  sed -e "s/route\(6\)*:\s\+//g" -e "s/:0000//g" | while read fb_ip ; 
do
    present=$(grep $fb_ip $iptables_rules );
    present_ipv6=$(grep -i $fb_ip $ip6tables_rules );
    ipv6=$(echo $fb_ip | grep ":");
    rule="-I INPUT -s $fb_ip -m comment --comment 'Facebook' -j REJECT";

    if [ ! "$ipv6" ] && [ ! "$present" ] ;
    then
	sudo iptables $rule ;
    elif [ "$ipv6" ] && [ ! "$present_ipv6" ];
    then
	sudo ip6tables $rule ;
    fi
done
